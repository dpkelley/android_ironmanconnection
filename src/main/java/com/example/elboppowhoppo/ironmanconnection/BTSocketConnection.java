package com.example.elboppowhoppo.ironmanconnection;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.os.Handler;
import android.widget.TextView;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class BTSocketConnection {

    TextView textView;
    MainActivity mainActivity;
    BTSocketConnection socketConnection;

    BluetoothAdapter mBluetoothAdapter;

    private BluetoothSocket mmSocket;
    private BluetoothDevice mmDevice;

    private OutputStream mmOutputStream;
    private InputStream mmInputStream;

    private Thread workerThread; //Each Bluetooth will have its own thread or virtual thread.
    private Thread original;

    private byte[] readBuffer;
    private int readBufferPosition;
    private volatile boolean stopWorker;
    boolean isReconnected;

    public boolean isOnline = false;

    private String lastPrint = "";
    private String lastCommand = "";
    private String deviceName = "";
    private String data = "";

    Handler handler = new Handler();

    UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"); //Standard SerialPortService ID
    final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    final byte delimiter = 10; //This is the ASCII code for a newline character;
    private CountDownTimer cTimer;


    public BTSocketConnection(TextView label, Activity activity, String name){
        deviceName = name;
        textView = label; mainActivity = (MainActivity)activity;
        socketConnection = BTSocketConnection.this;
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        original = Thread.currentThread();

    }




    void printLast(String data){
        if (!lastPrint.equals(data) && lastPrint != data) {
            textView.append(data + "\n");
            lastPrint = data;
        }

    }

    boolean deviceOpen(String device){


        if (bluetoothFound(device)) {
            try {
                openSocket();
                readDevice(false);
                return true;
            } catch (IOException ex) {

            }
        }
        return false;
    }

    boolean deviceReopened(String device){


        if (bluetoothFound(device)) {
            try {
                openSocket();
                return true;
            } catch (IOException ex) {

            }
        }
        return false;
    }



    boolean openSocket() throws IOException
    {
        Boolean isNull = mmSocket == null;
//        textView.append("null socket? " + isNull + "\n");

        if (mmSocket == null && !isOnline) {

            return getSocket("connecting...\n");

        }else if (mmSocket != null && !mmSocket.isConnected() && !isOnline){

            mmSocket.close();
            return getSocket("reconnecting...\n");

        }else if (mmSocket != null && !isOnline){

            mmSocket.close();
            return getSocket("reconnecting02...\n");


        }else if (mmSocket != null && mmSocket.isConnected()  && isOnline){

            textView.append("Already Connected...\n");

        }
        textView.append("Connected to "+mmDevice.getName()+"e\n");
        return false;
    }


    boolean getSocket(String message) throws IOException{
        mmSocket = mmDevice.createRfcommSocketToServiceRecord(uuid);
        mmSocket.connect();
        mmOutputStream = mmSocket.getOutputStream();
        mmInputStream = mmSocket.getInputStream();
       // textView.append(message);

        return mmSocket.isConnected();
    }


    Set<BluetoothDevice> getDEviceList(BluetoothAdapter mBluetoothAdapter){

        if(mBluetoothAdapter == null)
        {
            textView.append("No bluetooth adapter available\n");
        }

        if(!mBluetoothAdapter.isEnabled())
        {
            Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            mainActivity.startActivityForResult(enableBluetooth, 0);
            textView.append("Bluetooth already enabled\n");
        }
        return mBluetoothAdapter.getBondedDevices();

    }




    boolean bluetoothFound(String device)
    {
        Set<BluetoothDevice> pairedDevices = getDEviceList(mBluetoothAdapter);

        if(pairedDevices.size() > 0)
        {
            for(BluetoothDevice specificDevice : pairedDevices)
            {

                if (specificDevice.getName().contains(device)) {
                    mmDevice = specificDevice;
                    //textView.append(device + " found\n");

                    return mmDevice.getBondState() == 12 ;
                }


            }
        }
        return mmDevice.getBondState() == 12 ;
    }



    public String readDevice(boolean interrupt)
    {
        stopWorker = false;

        readBufferPosition = 0;
        readBuffer = new byte[1024];
        workerThread = getThreadStream(new Handler());

        workerThread.start();
        return data;
    }

    public String getDeviceName(){ return deviceName; }

    public Thread getMainThread(){ return original; }

    public Thread getWorkerThread(){ return workerThread; }

    public String getLastPrint(){  return lastPrint; }

    public void setLastPrint(String command){  lastPrint = command; }



    public void checkTimeout(final Handler handler, long seconds) {
        Runnable task = new Runnable() {public void run() {
            //


            if(isOnline)handler.post(new Runnable() {public void run() {

                //Thread.currentThread().setPriority(Thread.MIN_PRIORITY);
                System.out.println(deviceName+": "+"Timedout");
                isOnline = false;
                sendData("checkAlive"); //BTSocketConnection
                //join(workerThread, 500);
            }});
            if(!isOnline ) handler.post(new Runnable() {public void run() {
                //Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
                System.out.println(deviceName+": "+"Waiting for connect");
                sendData("checkAlive");

                //deviceOpen(deviceName); //BTSocketConnection
                //join(workerThread, 333);

            }});

        }};

        scheduler.scheduleWithFixedDelay(task, 10, seconds, TimeUnit.SECONDS);
    }





    Thread getThreadStream(final Handler handler){

        checkTimeout(handler, 4);

        return new Thread(new Runnable()
        {
            public void run()
            {
                while(!Thread.currentThread().isInterrupted() && !stopWorker) //!Thread.currentThread().isInterrupted()
                {

                    try
                    {
                        int bytesAvailable = mmInputStream.available();


                        if(bytesAvailable > 0)
                        {
                            byte[] packetBytes = new byte[bytesAvailable];
                            mmInputStream.read(packetBytes);
                            for(int i=0;i<bytesAvailable;i++)
                            {
                                byte b = packetBytes[i];

                                if(b == delimiter) //newline character \n
                                {

                                    byte[] encodedBytes = new byte[readBufferPosition];
                                    System.arraycopy(readBuffer, 0, encodedBytes, 0, encodedBytes.length);

                                    data = new String(encodedBytes, "UTF-8");
                                    readBufferPosition = 0;
                                    handler.post(new Runnable()
                                    {
                                        public void run()
                                        {

                                            mainActivity.RX_Handle(data, BTSocketConnection.this);


                                            // mainActivity.RX_Handle("_handshaking", BTSocketConnection.this);


                                        }
                                    });
                                }
                                else
                                {
                                    readBuffer[readBufferPosition++] = b;
                                }
                            }
                        }



                    }
                    catch (IOException ex)
                    {
                        System.out.println(ex);
                        stopWorker = true;
                    }
                }

            }
        }

        );

    }


    public void join(Thread t, long l){
        try {
            t.join(l);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void join(Thread t){
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public void sendData(String data) {

        if(mmSocket == null || !mmSocket.isConnected() || mmOutputStream == null ) {
            System.out.println("mmSocket.isConnected() = "+mmSocket.isConnected());
            ReconnectTask connect = new ReconnectTask();
            connect.execute(0);
        }
        lastCommand = data;
        data += "\n";
        try
        {
            //System.out.println("mmOutputStream == null = "+mmOutputStream == null);
            if( !(mmOutputStream == null) )
                mmOutputStream.write(data.getBytes());

        }
        catch (IOException ex) {
            closeBT();
        }
    }


    public void closeBT()
    {
        try {
            stopWorker = true;
            mmOutputStream.close();
            mmInputStream.close();
            mmSocket.close();
        } catch (Exception e) {

        }

    }

    private class ReconnectTask extends AsyncTask<Integer, Integer, Long> {

        protected Long doInBackground(Integer... ints) {

            isReconnected = deviceReopened(deviceName);

            if(isReconnected)handler.post(new Runnable() {public void run() {
                readDevice(false);
            }});

            return (long)0;
        }




        Thread getReconnectThread(){
            return Thread.currentThread();
        }
        //new ReconnectTask().execute(url1, url2, url3);
        //https://developer.android.com/reference/android/os/AsyncTask
    }


}