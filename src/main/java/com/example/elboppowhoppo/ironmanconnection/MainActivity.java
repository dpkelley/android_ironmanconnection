package com.example.elboppowhoppo.ironmanconnection;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import pl.droidsonroids.gif.GifImageView;


public class MainActivity extends Activity
{

    EditText myTextbox;
    TextView textView;
    TextView textView1;
    GifImageView gifImageView;

    CountDownTimer cTimer = null;

    int audioSessionId = -1;
    int turn = 0;
    int lastImage = 0;

    MediaPlayer mp;
    AudioManager audioManager;
    BTSocketConnection leftRepulsorConnection;
    BTSocketConnection rightRepulsorConnection;

    final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(2);
    ScheduledFuture<?> futureLogcatSchedule;
    ScheduledFuture<?> futureConnectSchedule;

    Thread main = Thread.currentThread();
    Thread oncreate = Thread.currentThread();

    Thread[] processThreads;
    Process process;
    BufferedReader bufferedReader;

    public void onboardLogcat(final long seconds) {

        final Handler handler = new Handler();

        final Runnable task = new Runnable() {public void run() {

            StringBuilder s = new StringBuilder("");

            String line = "";

                createBufferedProcess();

                try {
                while ((line = bufferedReader.readLine()) != null) {
                    final String result = line;
                    handler.post(new Runnable()
                    {
                        public void run()
                        {

                            textView1.append(result);

                        }
                    });

                    try {
                        for(Thread thread : processThreads)
                            thread.join(75


                            );
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                     }
            }
            catch(IOException e){ }


        }};

        futureLogcatSchedule = scheduler.scheduleWithFixedDelay(task, 0, seconds, TimeUnit.SECONDS);


    }

    void createBufferedProcess() {
        try {
        process = Runtime.getRuntime().exec("logcat -d");
        bufferedReader = new BufferedReader(
                new InputStreamReader(process.getInputStream()));
        } catch (IOException e) {
            // Handle Exception
        }
    }



    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);


        textView = findViewById(R.id.label2);
        textView1 = findViewById(R.id.textView1);
        textView.setMovementMethod(new ScrollingMovementMethod());

        gifImageView = findViewById(R.id.suit);


        leftRepulsorConnection = new BTSocketConnection(textView, this, "Repulsor Glove L");
        leftRepulsorConnection.deviceOpen(leftRepulsorConnection.getDeviceName()); //BTSocketConnection


        rightRepulsorConnection = new BTSocketConnection(textView, this, "Repulsor Glove R");
        rightRepulsorConnection.deviceOpen(rightRepulsorConnection.getDeviceName()); //BTSocketConnection


        processThreads = new Thread[]{leftRepulsorConnection.getWorkerThread(), rightRepulsorConnection.getWorkerThread(),
        main};


        String i = AudioManager.ACTION_HEADSET_PLUG;
        int j = AudioManager.SCO_AUDIO_STATE_CONNECTED;

        setListeners(leftRepulsorConnection);

        //onboardLogcat(60);
        //
        oncreate = Thread.currentThread();

        lastImage = R.drawable.trans_suit_lr;
        gifImageView.setImageResource(lastImage);
         //getOnlineStatus();
    }


    void getOnlineStatus(){

        final Handler handler = new Handler();
        final Drawable right = getResources().getDrawable(R.drawable.trans_suit_r);
        final Drawable left = getResources().getDrawable(R.drawable.trans_suit_l);
        final Drawable both = getResources().getDrawable(R.drawable.trans_suit_lr);
        final Drawable suit = getResources().getDrawable(R.drawable.trans_suit_);

        //gifImageView.setBackground(both);



        pl("DEBUG 0");

        final Runnable task = new Runnable() {public void run() {


                pl("DEBUG 1");

            try {
                oncreate.join(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (leftRepulsorConnection.isOnline && !rightRepulsorConnection.isOnline  && lastImage != R.drawable.trans_suit_r )
                {
                    handler.post(new Runnable()
                {
                    public void run()
                    {
                        pl("DEBUG 2");
                        lastImage = R.drawable.trans_suit_r;
                        gifImageView.setImageResource(lastImage);

                    }
                });
                }

                if (leftRepulsorConnection.isOnline && rightRepulsorConnection.isOnline   && lastImage != R.drawable.trans_suit_) {
                    handler.post(new Runnable()
                    {
                        public void run()
                        {
                            pl("DEBUG 3");
                            lastImage = R.drawable.trans_suit_;
                            gifImageView.setImageResource(lastImage);

                        }
                    });
                }

                if (!leftRepulsorConnection.isOnline && rightRepulsorConnection.isOnline && lastImage != R.drawable.trans_suit_l ) {
                    handler.post(new Runnable()
                    {
                        public void run()
                        {
                            pl("DEBUG 4");
                            lastImage = R.drawable.trans_suit_l;
                            gifImageView.setImageResource(lastImage);

                        }
                    });
                }

                if (!leftRepulsorConnection.isOnline && !rightRepulsorConnection.isOnline && lastImage != R.drawable.trans_suit_lr) {

                    handler.post(new Runnable()
                    {
                        public void run()
                        {
                            pl("DEBUG 5");
                            lastImage = R.drawable.trans_suit_lr;
                            gifImageView.setImageResource(lastImage);

                        }
                    });

                }


        }};
        futureConnectSchedule = scheduler.scheduleWithFixedDelay(task, 1, 4, TimeUnit.SECONDS);
    }

    void initialiseSounds(){
        //PLAY ON SPEAKER
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        audioManager.setMode(AudioManager.STREAM_RING);
        audioManager.setSpeakerphoneOn(true);
        audioManager.setBluetoothScoOn(false);

        mp = MediaPlayer.create(this, R.raw.systems_check);
        mp.setAudioStreamType(AudioManager.STREAM_MUSIC);

    }



    @Override
    public void onStop() {
        super.onStop();
        mp.stop();mp.reset();mp.release();
    }


    @Override
    public void onStart() {
        super.onStart();
        initialiseSounds();
    }

    @Override
    public void onResume() {
        super.onResume();
        initialiseSounds();
    }

    void setListeners(final BTSocketConnection connection) {

        Button openButton = findViewById(R.id.clear);
        Button sendButton = findViewById(R.id.send);
        Button closeButton = findViewById(R.id.close);

        //Play button
        closeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                play(R.raw.repulsorcharge, "repulsorblast.mp3", connection);
            }
        });


        //Send Button
        sendButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                connection.sendData("ping"); //BTSocketConnection

            }
        });

        //Clear Button
        openButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //onboardLogcat(5);
            }
        });

    }

        void playConnect(BTSocketConnection connection) {
        int [] efx = {R.raw.systems_check, R.raw.stark_industries2, R.raw.importing_all, R.raw.accessing};
        Random rnd = new Random();
        int track = rnd.nextInt(efx.length);
        play(efx[track], "online", connection);
    }



    public String RX_Handle(String raw, BTSocketConnection connection){  //Arduino data input

        String data = raw.substring(1);
        connection.printLast(connection.getDeviceName() +": "+data);

            switch (data){

            case "handshaking" : connection.sendData("ping"); break; //BTSocketConnection
            case "pingreply" : connection.isOnline = true;  playConnect(connection); break ; //BTSocketConnection
            case "deviceAlive" : connection.isOnline = true; break ; //BTSocketConnection
            case "charge" : play(R.raw.repulsorcharge, "charge", connection);  break; //BTSocketConnection
            case "dis" : play(R.raw.repulsordischarge, "charge", connection); break; //BTSocketConnection
            case "blast" : play(R.raw.repulsorblast, "charge", connection); break; //BTSocketConnection
            case "setup" : playEfx(connection); break; //BTSocketConnection

        }

        return data;
    }




        void playEfx(BTSocketConnection connection){
            int [] efx = {R.raw.efx1, R.raw.efx2, R.raw.efx3, R.raw.efx4, R.raw.efx5, R.raw.thinking_efx, R.raw.setup_complete};
            Random rnd = new Random();
            int track = rnd.nextInt(efx.length);
            play(efx[track], "setup", connection);
        }




    String play(int sound, String data, BTSocketConnection connection){


        if(mp.isPlaying()) {
            mp.stop();mp.reset();mp.release();
            mp = MediaPlayer.create(this, sound);
            mp.start();

            audioSessionId = mp.getAudioSessionId();
            connection.setLastPrint(data); //BTSocketConnection.lastComand
            turn = 1;
            return "1";
        }


        if(!mp.isPlaying()) {
            if(mp.getCurrentPosition() > 0){
            mp.stop();mp.reset();mp.release();}
            mp = MediaPlayer.create(this, sound);
            mp.start();
            audioSessionId = mp.getAudioSessionId();
            connection.setLastPrint(data);  //BTSocketConnection.lastComand
            turn = 1;
            return "1";
        }


        else if (mp.isPlaying()  && turn == 1){

            mp.stop();mp.reset();mp.release();
            if(mp.getCurrentPosition() != 0){
                mp.pause();
                mp.seekTo(0);}

            mp.start();
            connection.setLastPrint(data); //BTSocketConnection.lastComand
            turn = 1;
            return "1";
        }
        else if (!mp.isPlaying()  && turn == 1){
            turn = 0;
            return "1";
        }
        turn = 0;

        return "0";
    }


    void pl(String s){
        System.out.println(s);
    }

}